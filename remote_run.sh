#!/bin/bash

mkdir -p .cotps_auto
cd .cotps_auto || exit
curl https://gitlab.com/brien.d.mullins/public/-/raw/main/cotps.py > cotps.py
curl https://gitlab.com/brien.d.mullins/public/-/raw/main/cotps.sh > cotps.sh
curl https://gitlab.com/brien.d.mullins/public/-/raw/main/setup.sh > setup.sh

echo -e "
MY_COUNTRY_CODE=$1
MY_PHONE=$2
MY_PASS=$3
" > .env

#bash setup.sh
#pm2 start cotps.sh --name cotps